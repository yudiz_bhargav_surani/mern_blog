const controller = {};
const blogModel = require("@models/blogSchema");
const userModel = require("@models/usersSchema");
const { blogsPipeline } = require("@helper/admin/filterPipeline");
const {test} = require('@')
// const {test} = require('@models/test')
// const {test} = require('@models/test/test')
test()
controller.getUsers = async (req, res) => {
  try {
    const limit = 5;
    const page = req.query.page || 1;
    if (req.query?.sortby) {
      const pipeline = sort(req.query);
      const sortUsers = await userModel
        .aggregate(pipeline)
        .skip(page * limit - limit)
        .limit(limit);
      if (!sortUsers) return res.status(400).json({ message: "Something went wrong" });
      return res.status(200).json(sortUsers);
    }
    if (req.query?.search) {
      const pipeline = search(req.query);
      const searchUsers = await userModel
        .aggregate(pipeline)
        .skip(page * limit - limit)
        .limit(limit);
      if (!searchUsers.length) return res.status(200).json({ message: "Result not found" });
      if (!searchUsers) return res.status(400).json({ message: "Something went wrong" });
      return res.status(200).json(searchUsers);
    }
    const users = await userModel.find();
    if (!users) return res.status(400).json({ message: "Something went wrong" });
    return res.status(200).json(users);
  } catch (error) {
    return res.status(500).json({ message: "Error" + error.message });
  }
};
controller.deleteUser = async (req, res) => {
  const { userId } = req.body;
  try {
    const response = await userModel.deleteOne({ _id: userId });
    if (!response) return res.status(400).json({ message: "Something went wrong" });
    return res.status(200).json({ message: "SuccessFully deleted user" });
  } catch (error) {
    return res.status(500).json({ message: "Error" + error.message });
  }
};

controller.blogs = async (req, res) => {
  try {
    const pipeline = blogsPipeline(req.query);
    const blogs = await blogModel.aggregate(pipeline);
    if (!blogs[0].data.length) return res.status(200).json({ message: "Result not found" });
    if (!blogs) return res.status(400).json({ message: "Something went wrong" });
    return res.status(200).json(blogs);
  } catch (error) {
    return res.status(500).json({ message: "Error" + error });
  }
};

module.exports = controller;
