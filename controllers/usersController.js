const controller = {};
const { generateToken, verifyToken, forgotPasswordToken } = require("../utils/token/jwt");
const { comparePassword, hashPassword } = require("../utils/bcrypt/hashPassword");
const userModel = require("../models/usersSchema");
const { uploadFile, deleteFile } = require("../utils/s3/s3");
const sendEmail = require("../utils/sendEmail/sendEmail");

controller.signUp = async (req, res, next) => {
  const { sUserName, sEmail, sPassword, nMobile, sGender, sRole } = req.body;
  const hashedPass = await hashPassword(sPassword);
  if (req.file) {
    var profileImage = await uploadFile(req.file);
  }
  const userData = {
    sUserName,
    sEmail,
    sPassword: hashedPass,
    nMobile,
    sGender,
    aRole: sRole || "user",
    sProfileImage: profileImage?.Location || "",
  };
  try {
    if (await userModel.findOne({ sEmail })) return res.status(400).json({ message: "Email already register" });
    const user = new userModel(userData);
    const token = await generateToken(user._id);
    user.aTokens.push({ token });
    const response = await user.save();
    return res.status(201).json({ message: "Successfully register", response, token });
  } catch (error) {
    unlinkFile(req.file?.path);
    return res.status(500).json({ message: "Error" + error.message });
  }
};

controller.logIn = async (req, res) => {
  const { sEmail, sPassword } = req.body;
  try {
    const user = await userModel.findOne({ sEmail });
    if (!user) return res.status(400).json({ message: "User not exits" });
    const isMatch = await comparePassword(sPassword, user.sPassword);
    if (!isMatch) return res.status(400).json({ message: "password not match" });
    const token = await generateToken(user._id);
    if (user.aTokens.length > 4) user.aTokens.shift();
    user.aTokens.push({ token });
    await user.save();
    return res.status(200).json({ message: "Successfully login", token });
  } catch (error) {
    return res.status(500).json({ message: "Error" + error.message });
  }
};

controller.changePassword = async (req, res) => {
  const { sCurrentPassword, sNewPassword } = req.body;
  const { _id, sPassword } = req.user;
  try {
    const isMatch = await comparePassword(sCurrentPassword, sPassword);
    if (!isMatch) return res.status(400).json({ message: "password not match" });
    const hashedPass = await hashPassword(sNewPassword);
    await userModel.updateOne({ _id }, { sPassword: hashedPass });
    return res.status(200).json({ message: "password changed" });
  } catch (error) {
    return res.status(500).json({ message: "Error :" + error.message });
  }
};

controller.profile = async (req, res) => {
  const { id } = req.user;
  try {
    const profile = await userModel.findById(id);
    if (!profile) return res.status(400).json({ message: "Something went wrong" });
    return res.status(200).json(profile);
  } catch (error) {
    return res.status(500).json({ message: "Error :" + error.message });
  }
};

controller.editProfile = async (req, res) => {
  const { sUserName, sEmail, nMobile, sGender } = req.body;
  const { _id } = req.user;

  if (req.file) {
    var profileImage = await uploadFile(req.file);
  }
  if (req.user.sProfileImage)
    if (req.user.sProfileImage !== profileImage?.Location) {
      await deleteFile(req.user.sProfileImage);
    }
  try {
    if (sEmail !== req.user.sEmail) {
      if (await userModel.findOne({ sEmail }))
        return res.status(400).json({ message: "Email already register, try different" });
    }
    await userModel.updateOne(
      { _id },
      { sUserName, sEmail, nMobile, sGender, sProfileImage: profileImage?.Location || req.user.sProfileImage }
    );
    return res.status(201).json({ message: "Successfully updated" });
  } catch (error) {
    unlinkFile(req.file?.path);
    return res.status(500).json({ message: "Error :" + error.message });
  }
};

controller.forgotPassword = async (req, res) => {
  const { sEmail } = req.body;
  try {
    const user = await userModel.findOne({ sEmail });
    if (!user) return res.status(404).send({ message: "User not found" });
    const token = await forgotPasswordToken(user._id);
    const link = `${req.protocol}://${req.get("host")}/user/resetpassword/${token}`;
    const response = await sendEmail(sEmail, link);
    return res.status(200).send({ message: "Password reset link has been successfully sent to your inbox" });
  } catch (error) {
    return res.status(500).json({ message: "Error :" + error.message });
  }
};
controller.resetPossword = async (req, res) => {
  const { sPassword } = req.body;
  if (!sPassword) return res.status(400).json({ message: "please enter password" });
  const { token } = req.params;
  try {
    const decoded = verifyToken(token);
    const hashedPass = await hashPassword(sPassword);
    const response = await userModel.updateOne({ _id: decoded.id }, { sPassword: hashedPass });
    if (!response.modifiedCount) return res.status(400).json({ message: "something went wrong" });
    return res.status(200).json({ message: "password changed" });
  } catch (error) {
    return res.status(500).json({ message: "Error :" + error.message });
  }
};

controller.logOut = async (req, res) => {
  try {
    req.user.aTokens = req.user.aTokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();
    return res.status(200).json({ message: "Successfully logout" });
  } catch (e) {
    return res.status(500).send();
  }
};

module.exports = controller;
