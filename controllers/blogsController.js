const controller = {};
const blogModel = require("../models/blogSchema");
const { uploadFile, unlinkFile } = require("../utils/s3/s3");

controller.postBlog = async (req, res) => {
  const { sTitle, sDescription, dPublishedDate } = req.body;
  const { sUserName, _id } = req.user;
  if (!sTitle) return res.status(400).json({ message: "Please enter sTitle" });
  if (!sDescription) return res.status(400).json({ message: "Please enter sDescription" });

  if (req.file) {
    var coverImage = await uploadFile(req.file);
  }
  const blogData = {
    sTitle,
    sDescription,
    dPublishedDate: dPublishedDate || new Date(),
    sUserName,
    sUserId: _id,
    sCoverImage: coverImage?.Location || "",
  };
  try {
    const blog = new blogModel(blogData);
    const response = await blog.save();
    if (!response) return res.status(400).json({ message: "something went wrong" });
    return res.status(201).json({ message: "Successfully Posted Blog", response });
  } catch (error) {
    unlinkFile(req.file?.path);
    return res.status(500).json({ message: "Error" + error });
  }
};

controller.getBlog = async (req, res) => {
  const { _id } = req.user;
  try {
    const limit = 5;
    const page = req.query.page || 1;
    const blogs = await blogModel
      .find({ sUserId: _id })
      .skip(page * limit - limit)
      .limit(limit);
    if (!blogs.length) return res.status(200).json({ message: "Result not found" });
    return res.status(200).json({ blogs });
  } catch (error) {
    return res.status(500).json({ message: "Error" + error });
  }
};

controller.likeBlog = async (req, res) => {
  const { sBlogId } = req.body;
  const { _id } = req.user;
  if (!sBlogId) return res.status(400).json({ message: "Please enter sBlogId" });
  try {
    let blog = await blogModel.findById({ _id: sBlogId });
    const isLike = blog.aLikes.includes(_id);
    if (!isLike) {
      blog.aLikes.addToSet(_id);
      const response = await blog.save();
      if (!response) return res.status(400).json({ message: "something went wrong" });
      return res.status(201).json({ message: "Like" });
    }
    blog.aLikes.pull(_id);
    const response = await blog.save();
    if (!response) return res.status(400).json({ message: "something went wrong" });
    return res.status(201).json({ message: "unLike" });
  } catch (error) {
    return res.status(500).json({ message: "Error" + error });
  }
};

controller.allBlogs = async (req, res) => {
  try {
    const date = new Date();
    const limit = 5;
    const page = req.query.page || 1;
    if (req.query?.order) {
      const order = parseInt(req.query.order);
      const blogs = await blogModel
        .aggregate([
          { $match: { dPublishedDate: { $lte: date } } },
          { $set: { nLike: { $size: "$aLikes" } } },
          { $sort: { nLike: order } },
        ])
        .skip(page * limit - limit)
        .limit(limit);
      if (!blogs.length) return res.status(200).json({ message: "Result not found" });
      return res.status(200).json(blogs);
    }
    const blogs = await blogModel
      .aggregate([
        { $match: { dPublishedDate: { $lte: date } } },
        { $set: { nLike: { $size: "$aLikes" } } },
        { $sort: { nLike: -1 } },
      ])
      .skip(page * limit - limit)
      .limit(limit);
    if (!blogs.length) return res.status(200).json({ message: "Result not found" });
    return res.status(200).json(blogs);
  } catch (error) {
    return res.status(500).json({ message: "Error" + error });
  }
};
module.exports = controller;
