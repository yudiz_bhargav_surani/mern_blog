const json2xls = require('utility_func/json2xls');
const  fs = require('fs');
function jsonToXls(params) {
    console.log('****start***')
    let data = fs.readFileSync('./jsonformatter.json')
    let data2 = fs.readFileSync('./scorepoints.json')
    
    data = JSON.parse(data)
    data2 = JSON.parse(data2)
    const result = []
    
    for (const point of data) {
        const obj = {}
       if(point.eCategory === 'BASKETBALL') {
        const eng = data2.find(e => e.sKey === point.sKey)
            obj.Name_English = eng.sName
            obj.Name_Brazil = point.sName
            result.push(obj)
       }
    }
    console.log(result)
    var xls = json2xls(result);
    fs.writeFileSync('BASKETBALL.xlsx', xls, 'binary');
    console.log('****stop***')
}

// jsonToXls()