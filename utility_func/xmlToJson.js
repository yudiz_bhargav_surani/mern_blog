var xls = require('read-excel-file/node');
const fs = require('fs')
// const points = fs.readFileSync('./scorepoints.json')
// const scorePoints = JSON.parse(points)

async function xmlToJson() {
    try {    
        // const engGeneral = require('./english/general')
        const data = await xls('./xl_files/Brazilian Names - Bots.xlsx')
        const lastName = new Set()
        const femaleName = new Set()
        const maleName = new Set()
        for(const scoreName of data){
            // ********** scorePoint code ********
            // const exists = scorePoints.find(({sName}) => sName === scoreName[0] )
            // if(engGeneral[scoreName[0]] && engGeneral[scoreName[0]].startsWith('##')){
            //     engGeneral[scoreName[0]] = scoreName[2] ? `## ${scoreName[2]}`.replace(/',/g,"") : scoreName[2]
            // } else{
            //     engGeneral[scoreName[0]] =scoreName[2] ?  scoreName[2].replace(/',/g,"") : scoreName[2]
            // }
            // if(exists){
            //     exists.sName = scoreName[1]
            //     bScore.push(exists)
            // }

            // ************Name Code ************
            lastName.add(scoreName[2])
            if(scoreName[0] === 'Male') { maleName.add(scoreName[1]) } 
            else { femaleName.add(scoreName[1]) } 
        }
        console.log(maleName)
        const engGeneral = {
            maleFirstNames : [...maleName],
            femaleFirstNames : [...femaleName],
            lastNames:[...lastName]
        }
        // console.log(engGeneral)
        fs.writeFileSync('BrazilName.js',JSON.stringify(engGeneral))
    } catch (error) {
        console.log(error)
    }
}
// xmlToJson()

