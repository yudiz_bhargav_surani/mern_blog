const search = (query) => {
  const regex = new RegExp(query.searchby);
  if (query.search) {
    const uPipeline = [{ $match: { [query.search]: { $regex: regex, $options: "si" } } }];
    return uPipeline;
  }
};
const sort = (query) => {
  query.order = !query.order ? -1 : parseInt(query.order);
  if (query.sortby) {
    const uPipeline = [{ $sort: { [query.sortby]: query.order } }];
    return uPipeline;
  }
};

const blogsPipeline = (query) => {
  let pipeline = [];
  const order = !query.order ? -1 : parseInt(query.order);
  const limit = parseInt(query.limit || 5);
  const page = query.page || 1;
  const skip = page * limit - limit;
  const date = new Date();

  if (query.published === "true") {
    pipeline.push({ $match: { dPublishedDate: { $lte: date } } });
  }
  if (query.published === "false") {
    pipeline.push({ $match: { dPublishedDate: { $gt: date } } });
  }

  if (query.search) {
    const regex = new RegExp(query.searchBy);
    pipeline.push({ $match: { [query.search]: { $regex: regex, $options: "si" } } });
  }

  if (query.startDate) {
    const startdate = new Date(query.startDate);
    const endDate = new Date(query.endDate);
    pipeline.push({ $match: { [query.dateField]: { $gte: startdate, $lte: endDate } } });
  }

  if (query.days) {
    let searchDate = new Date();
    searchDate.setDate(searchDate.getDate() - parseInt(query.days));
    pipeline.push({ $match: { dPublishedDate: { $gte: searchDate, $lte: date } } });
  }

  pipeline.push(
    { $set: { nLike: { $size: "$aLikes" } } },
    { $sort: { nLike: order } },
    {
      $facet: {
        data: [{ $skip: skip }, { $limit: limit }],
        metaData: [{ $count: "count" }, { $addFields: { page: { $ceil: { $divide: ["$count", limit] } } } }],
      },
    }
  );
  return pipeline;
};

module.exports = { search, sort, blogsPipeline };
