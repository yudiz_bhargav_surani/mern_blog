require('module-alias/register')
require('./alias')
const express = require("express");
const app = express();
require("./db/databaseConnect");
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
require("dotenv").config();
const routes = require("./routes/routes");

app.use("/", routes);

app.listen(process.env.PORT || 3000, function () {
  console.log("server listening on port %d in %s mode", this.address().port, app.settings.env);
});

// require('./utility_func/translate')
// require('./utility_func/json2xls')
// require('./utility_func/xmlToJson')
// require('./utility_func/BrazilName')


