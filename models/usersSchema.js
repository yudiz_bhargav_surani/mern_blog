const mongoose = require("mongoose");
const blogModel = require("./blogSchema");

const usersSchema = new mongoose.Schema(
  {
    sUserName: {
      type: String,
      require: true,
      trim: true,
    },
    sEmail: {
      type: String,
      require: true,
      trim: true,
      unique: true,
    },
    nMobile: {
      type: Number,
      require: true,
      trim: true,
    },
    sPassword: {
      type: String,
      require: true,
      trim: true,
    },
    sGender: {
      type: String,
      require: true,
      trim: true,
    },
    sProfileImage: {
      type: String,
    },
    aRole: [
      {
        type: String,
        default: "user",
        enum: ["user", "admin"],
      },
    ],
    aTokens: [
      {
        token: {
          type: String,
        },
      },
    ],
  },
  { timestamps: true }
);
const usersModel = mongoose.model("users", usersSchema);

module.exports = usersModel;
