const mongoose = require("mongoose");

const blogSchema = new mongoose.Schema(
  {
    sUserId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    sUserName: {
      type: String,
      required: true,
    },
    sTitle: {
      type: String,
      trim: true,
      required: true,
    },
    sDescription: {
      type: String,
      required: true,
    },
    dPublishedDate: {
      type: Date,
    },
    aLikes: [{ type: mongoose.Schema.Types.ObjectId }],
    sCoverImage: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const blogModel = mongoose.model("blogs", blogSchema);

module.exports = blogModel;
