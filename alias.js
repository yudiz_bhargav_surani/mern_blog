const moduleAlias = require('module-alias')
const fs = require("fs")

function containsDot(str) {
    return /\.(?=[A-Za-z])/g.test(str);
  }
const utils = fs.readdirSync(__dirname )
let alias = {}
utils.map((dir) => {
    if(!containsDot(dir) || dir === 'node_modules'){  
        alias[`@${dir}`] = `${__dirname}/${dir}` 
  }
})
// console.log(alias)
moduleAlias.addAliases(alias)
// moduleAlias.addPath(__dirname + "/models/" )
// moduleAlias(__dirname + '/package.json')
// moduleAlias.addPath(__dirname + '/models')
moduleAlias()


// "_moduleAliases": {
//     "@root"      : ".", 
//     "@models"      : "./models"
//   },

// "_moduleDirectories": ["/alias.js"],