const router = require("express").Router();

const users = require("./usersRouter");
const blogs = require("./blogsRouter");
const admin = require("./adminRouter");

router.use("/user", users);
router.use("/blog", blogs);
router.use("/admin", admin);

router.all("*", (req, res) => {
  try {
    return res.status(400).json({ message: "Route not found!" });
  } catch (error) {
    return res.status(404).json({ message: error.message });
  }
});

module.exports = router;
