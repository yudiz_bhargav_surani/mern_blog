const router = require("express").Router();
const middleware = require("../middlewares/userAuth");
const controller = require("../controllers/blogsController");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });

router.post("/postblog", upload.single("coverImage"), middleware.authorizationToken, controller.postBlog);
router.get("/userblog", middleware.authorizationToken, controller.getBlog);
router.post("/likeblog", middleware.authorizationToken, controller.likeBlog);
router.get("/blogs", middleware.authorizationToken, controller.allBlogs);

module.exports = router;
