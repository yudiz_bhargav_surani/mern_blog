const router = require("express").Router();
const controller = require("../controllers/usersController");
const middleware = require("../middlewares/userAuth");
const validators = require("../validator/userValidation");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });

router.post("/signup", upload.single("profileImage"), validators.signUp, controller.signUp);
router.post("/login", validators.logIn, controller.logIn);
router.post("/changepassword", validators.changePassword, middleware.authorizationToken, controller.changePassword);
router.get("/profile", middleware.authorizationToken, controller.profile);
router.post(
  "/editprofile",
  middleware.authorizationToken,
  upload.single("profileImage"),
  validators.editProfile,
  controller.editProfile
);
router.get("/logout", middleware.authorizationToken, controller.logOut);
router.post("/forgotpassword", validators.forgotPassword, controller.forgotPassword);
router.post("/resetpassword/:token", controller.resetPossword);

module.exports = router;
