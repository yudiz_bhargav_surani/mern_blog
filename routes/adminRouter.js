const router = require("express").Router();
const middleware = require("../middlewares/userAuth");
const validators = require("../validator/userValidation");
const userController = require("../controllers/usersController");
const adminController = require("../controllers/adminController");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });

router.post(
  "/createuser",
  upload.single("profileImage"),
  validators.signUp,
  middleware.authorizationToken,
  middleware.isAdmin,
  userController.signUp
);
router.get("/users", middleware.authorizationToken, middleware.isAdmin, adminController.getUsers);
router.post("/deleteuser", middleware.authorizationToken, middleware.isAdmin, adminController.deleteUser);
router.get("/blogs", middleware.authorizationToken, middleware.isAdmin, adminController.blogs);

module.exports = router;
