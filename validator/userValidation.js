const validators = {};
const { unlinkFile } = require("../utils/s3/s3");

validators.signUp = async (req, res, next) => {
  const { sUserName, sEmail, sPassword, nMobile, sGender } = req.body;
  if (req?.file) {
    if (
      !(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/jpg" || req.file.mimetype === "image/png")
    ) {
      await unlinkFile(req.file.path);
      return res.status(400).json({ message: "Image formate should be jpeg,jpg or png" });
    }
  }
  try {
    if (!sUserName) throw new Error("Please enter UserName");
    if (!sEmail) throw new Error("Please enter sEmail");
    if (!sPassword) throw new Error("Please enter sPassword");
    if (!nMobile) throw new Error("Please enter nMobile");
    if (!sGender) throw new Error("Please enter sGender");
    const emailregex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailregex.test(sEmail)) throw new Error("Please enter valid email");
    const phoneRegex = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/;
    if (!phoneRegex.test(nMobile)) throw new Error("Please enter valid mobile");
    next();
  } catch (e) {
    unlinkFile(req.file?.path);
    return res.status(400).json({ message: e.message });
  }
};

validators.logIn = async (req, res, next) => {
  const { sEmail, sPassword } = req.body;
  try {
    if (!sEmail) throw new Error("Please enter sEmail");
    if (!sPassword) throw new Error("Please enter sPassword");
    const emailregex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailregex.test(sEmail)) throw new Error("Please enter valid email");
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
  next();
};

validators.changePassword = async (req, res, next) => {
  const { sCurrentPassword, sNewPassword, sConfirmPassword } = req.body;
  try {
    if (!sNewPassword) throw new Error("Please enter sNewPassword");
    if (!sCurrentPassword) throw new Error("Please enter sCurrentPassword");
    if (!sConfirmPassword) throw new Error("Please enter sConfirmPassword");
    if (sNewPassword !== sConfirmPassword) throw new Error("confirm password and new password not matched");
    next();
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

validators.forgotPassword = async (req, res, next) => {
  const { sEmail } = req.body;
  try {
    if (!sEmail) throw new Error("Please enter sEmail");
    const emailregex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailregex.test(sEmail)) throw new Error("Please enter valid email");
    next();
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

validators.editProfile = (req, res, next) => {
  const { sEmail, nMobile } = req.body;
  try {
    const emailregex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailregex.test(sEmail)) throw new Error("Please enter valid email");
    const phoneRegex = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/;
    if (!phoneRegex.test(nMobile)) throw new Error("Please enter valid mobile");
    next();
  } catch (error) {
    unlinkFile(req.file?.path);
    return res.status(400).json({ message: error.message });
  }
};

module.exports = validators;
