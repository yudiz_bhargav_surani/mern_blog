const sgMail = require("@sendgrid/mail");
require("dotenv").config();
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendEmail = async (receiver, link) => {
  try {
    const data = {
      to: receiver,
      from: process.env.SENDGRID_EMAIL_ID,
      subject: "MERN blog reset password",
      html: `
      <div>Click the link below to reset your password</div><br/>
      <div>${link}</div>
      `,
    };
    return await sgMail.send(data);
  } catch (e) {
    return new Error(e);
  }
};

module.exports = sendEmail;
