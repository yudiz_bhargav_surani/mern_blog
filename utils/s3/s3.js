/* eslint-disable no-undef */
require("dotenv").config();
const AWS = require("aws-sdk");
const fs = require("fs");
const util = require("util");
const unlinkFile = util.promisify(fs.unlink);
const s3 = new AWS.S3({
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_ACCESS_SECRET_KEY,
  region: process.env.S3_RIGION,
});

async function uploadFile(file) {
  const type = file.originalname.split(".")[1];
  const fileStream = fs.createReadStream(file.path);
  const uploadParams = {
    Bucket: process.env.S3_BUCKET_NAME,
    Body: fileStream,
    Key: `${file.filename}.${type}`,
    ContentType: "image/jpeg",
  };
  try {
    const response = await s3.upload(uploadParams).promise();
    await unlinkFile(file.path);

    return response;
  } catch (error) {
    await unlinkFile(file.path);
  }
}

async function deleteFile(file) {
  const fileName = file.split("/")[3];
  const params = {
    Bucket: process.env.S3_BUCKET_NAME,
    Key: fileName,
  };
  const response = await s3.deleteObject(params).promise();
  return response;
}

async function deleteManyFile(files) {
  // const fileName = file.split("/")[3];
  // const files = files.forEach((file) => {
  //   const fileName = file.Key.split("/")[3];
  //   return { Key: fileName };
  // });
  // const params = {
  //   Bucket: process.env.S3_BUCKET_NAME,
  //   Delete: {
  //     Objects: files,
  //     Quiet: false,
  //   },
  // };
  // const response = await s3.deleteObject(params).promise();
  // return response;
}
module.exports = { uploadFile, unlinkFile, deleteFile };
